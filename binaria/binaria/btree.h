#ifndef BTREE_H
#define BTREE_H

#include <sstream>

#include <iostream>
#include "node.h"
//#include "../libs/btreeview.h"

class btree
{

public:
    bnode * root{nullptr};

    btree()
    {
    }

    bnode * _clone(bnode * from){
        if(from == nullptr)
            return nullptr;
        bnode * node = new bnode(from->value);
        node->left = _clone(from->left);
        node->right = _clone(from->right);
        return node;
    }

    btree(const btree &other){
        this->root = _clone(other.root);
    }

    ~btree(){
        this->clear();
    }

    void gambi_fill(){
        root = new bnode(6);
        root->left = new bnode(3);
        root->left->right = new bnode(4);
        auto nove = new bnode(9);
        root->right = nove;
        nove->left = new bnode(7);
        nove->right = new bnode(10);
        nove->right->right = new bnode(11);
    }

    int _size(bnode * node){
        if(node == nullptr)
            return 0;
        return 1 + _size(node->left) + _size(node->right);
    }

    //retorna a quantidade de elementos da arvore
    int size(){
        if(root == nullptr)
            return 0;
        return _size(root);
    }

    int _altura(bnode * node){
        if(node == nullptr)
            return 0;
        return 1 + std::max(_altura(node->left), _altura(node->right));
    }

    int altura(){
        if(root == nullptr)
            return 0;
        return _altura(root);
    }

    void _print(bnode * node){
        if(node == nullptr)
            return;

        _print(node->left);
        std::cout << node->value << " ";
        _print(node->right);

    }

    void print(){
        _print(root);
        std::cout << "\n";
    }


    void _printTree(bnode * node, int altura){
        if(node == nullptr){
            std::cout << std::string(2 * altura, ' ') << "#" << std::endl;
            return;
        }
        _printTree(node->left, altura + 1);

        std::cout << std::string(2 * altura, ' ') << node->value << std::endl;

        _printTree(node->right, altura + 1);
    }

    void printTree(){
        std::cout << "########\n";
        _printTree(root, 0);
    }

    bnode * _insert(bnode * node, int value){
        if(node == nullptr)
            return new bnode(value);
        if(value > node->value)
            node->right = _insert(node->right, value);
        if(value < node->value)
            node->left = _insert(node->left, value);
        return node;
    }

    void insert(int value){
        if(root == nullptr)
            root = new bnode(value);
        else
            _insert(root, value);

    }

    void _serialize(std::stringstream &saida, bnode * n){
        if(n == nullptr){
            saida << "# ";
            return;
        }
        saida << n->value << " ";
        _serialize(saida, n->left);
        _serialize(saida, n->right);

    }

    std::string serialize(){
        std::stringstream saida;
        _serialize(saida, root);
        return saida.str();
    }

    void _load(bnode * node, std::stringstream &str){
        std::string data;
        str >> data;
        if(data != "#"){
            int i;
            std::stringstream(data) >> i;
            node->left = new bnode(i);
            _load(node->left, str);
        }
        str >> data;
        if(data != "#"){
            int i;
            std::stringstream(data) >> i;
            node->right = new bnode(i);
            _load(node->right, str);
        }
    }

    void load(std::string tree){
        this->clear();
        std::stringstream str(tree);
        std::string data;

        str >> data;
        if(data == "#")
            return;

        int i;
        std::stringstream(data) >> i;
        root = new bnode(i);
        _load(root, str);
    }

    void _clear(bnode * node){
        if(node == nullptr)
            return;
        _clear(node->left);
        _clear(node->right);
        delete(node);
    }

    void clear(){
        _clear(root);
        root = nullptr;
    }



    bnode * _find_sucessor(bnode * node){
        if(node->left == nullptr)
            return node;
        return _find_sucessor(node->left);
    }

    bnode * _remove(bnode * node, int value){
        if(node == nullptr)
            return nullptr;

        if(value > node->value)
            node->right = _remove(node->right, value);

        if(value < node->value)
            node->left = _remove(node->left, value);

        if(node->value == value){
            if(node->right == nullptr && node->left == nullptr){
                delete(node);
                return nullptr;
            }
            if(node->right == nullptr and node->left != nullptr){
                auto bak = node->left;
                delete(node);
                return bak;
            }
            if(node->right != nullptr and node->left == nullptr){
                auto bak = node->right;
                delete(node);
                return bak;
            }
            bnode * suc = _find_sucessor(node->right);
            int novo_value = suc->value;
            _remove(node, novo_value);
            node->value = novo_value;
        }
        return node;
    }

    bool remove(int value){
        if(find(value) != nullptr){
            root = _remove(root, value);
            return true;
        }
        return false;
    }

    //busca o no
    bnode * _find(bnode * node, int value){
        if(node == nullptr)
            return nullptr;
        if(value > node->value)
            return _find(node->right, value);
        if(value < node->value)
            return _find(node->left, value);
        return node;
    }

    //busca o no
    bnode * find(int value){
        return _find(root, value);
    }
};















#endif // BTREE_H
