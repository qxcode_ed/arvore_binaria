#ifndef NODE_H
#define NODE_H

struct nodeview{
    int dx_d{0};
    int la_d{0};
    int dx_e{0};
    int la_e{0};
    char color{'g'};
};

struct bnode : public nodeview{
    bnode(const int value){
        this->value = value;
    }

    int value;
    bnode *left{nullptr};
    bnode *right{nullptr};
};
#endif // NODE_H
