#include <iostream>
#include "binaria/btree.h"
#include "libs/btreeview.h"

using namespace std;

int main()
{
    srand(time(NULL));

    my_player->set_buffer_size(100);
    btree tree;
    int value;

    for(int i = 0; i < 1000; i++){
        value = rand();
        tree.insert(value % 30);
        my_treeview->push(tree.root, "Inserido " + to_string(value % 30));
        value = rand();
        tree.remove(value % 30);
        my_treeview->push(tree.root, "Removido " + to_string(value % 30));
    }

    my_treeview->wait();




    return 0;
}

