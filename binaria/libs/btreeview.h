#ifndef BTREEVIEW_H
#define BTREEVIEW_H

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

//#include "../player/mplayer.h"
//#include "../player/sfline.h"
//#include "../player/sftext.h"
//#include "../player/colormap.h"
//#include "../player/mywindow.h"

#include "player.h"

#include "../binaria/node.h"

#include <queue>
#include <sstream>
#include <vector>



#define my_treeview BTreeView::get_instance()

class BTreeView{

private:
    int raio;
    float delta;
    int dvert;
    sf::Color cor_nodo;
    sf::Color cor_borda;
    sf::Color cor_linha = sf::Color::White;
    sf::Color cor_fundo;
    sf::RenderTexture painel;


    BTreeView():
        raio(20),
        cor_nodo(153, 204, 0),
        cor_borda(77, 153, 0),
        cor_fundo(77, 77, 77)
    {
        delta = 1.2 * raio;
        dvert = 2 * raio;
        painel.create(800, 600);
    }

public:

    static BTreeView * get_instance(){
        static BTreeView bview;
        return &bview;
    }

    void push(bnode * root, std::string msg = ""){
        if(my_window->isOpen()){
            my_player->_push(mega_draw(root, msg));
        }
    }

    void wait(){
        my_player->wait();
    }

private:

    void _carregarEspacos(bnode * n)
    {
        if (n == nullptr)
            return;
        _carregarEspacos(n->left);
        _carregarEspacos(n->right);
        if (n->left != nullptr) {
            n->dx_e = n->left->la_d + delta;
            n->la_e = n->dx_e + n->left->la_e;
        }
        if (n->right != nullptr) {
            n->dx_d = n->right->la_e + delta;
            n->la_d = n->dx_d + n->right->la_d;
        }
    }

    void _draw(bnode * n, sf::Vector2f coord)
    {
        if (n == nullptr)
            return;
        sf::CircleShape noShape(raio);
        noShape.setOrigin(sf::Vector2f(raio, raio));
        noShape.setPosition(coord);
        sf::Color color = ColorMap::get_instance()->get(n->color);
        noShape.setFillColor(color);
        noShape.setOutlineColor(cor_borda);
        noShape.setOutlineThickness(1);

        sf::Vector2f left = coord + sf::Vector2f(-n->dx_e, dvert);
        sf::Vector2f right = coord + sf::Vector2f( n->dx_d, dvert);

        // Desenhando linhas
        if (n->left != nullptr)
            painel.draw(sfLine(coord, left, 3, cor_linha));
        if (n->right != nullptr)
            painel.draw(sfLine(coord, right, 3, cor_linha));

        // Desenhando no
        painel.draw(noShape);

        // Desenhando valor do no
        sfText text(coord, std::to_string(n->value), sf::Color::Black, 20);
        sf::Vector2f v(text.getGlobalBounds().width/2, 10 + text.getGlobalBounds().height/2);
        text.setPosition(coord - v);
        painel.draw(text);

        _draw(n->left, left);
        _draw(n->right, right);
    }

public:
    sf::Texture mega_draw(bnode * root, const std::string &msg = ""){
        painel.clear();
        _carregarEspacos(root);
        sf::Vector2f coord(my_window->getSize().x/2, 70);
//        sf::Vector2f coord(400, 70);
        _draw(root, coord);
        painel.draw(sfText(sf::Vector2f(0, my_window->getSize().y - 20), msg, my_player->color_front));
        painel.display();
        return painel.getTexture();
    }

};

#endif // BTREEVIEW_H
